## Games

|Published|Source|
|---|---|
|<https://marlinmr.gitlab.io/ants/>|<https://gitlab.com/marlinmr:ants>|
|<https://marlinmr.gitlab.io/legacyants/>|<https://gitlab.com/marlinmr:legacyants>|
|<https://marlinmr.gitlab.io/aquarium/>|<https://gitlab.com/marlinmr:aquarium>|


## Projects

|Published|Source|
|---|---|
|<https://marlinmr.gitlab.io/reference-guide/>|<https://gitlab.com/marlinmr:reference-guide>|


## Key

```bash
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGZLnosBEACbMJo6vG/N7IVVRh31AQoCRpoLEDFyDk1pVfv1BMBce759XCui
fYGuf927ZpZuolcjUG8oRLi4h7EGLPFjMYAvFT9XLrivQTOCTrmSxla6660/VVeL
ujdXD4gWG1D91Ek2gby/3YEK7En7zz/4/bVJ4yeObFypOat+MXph5xKPED178KkP
ciM9ebv9PhYOOHAHzPKQRcbHQcR8vc3g1GrE8bKsMLoAgtGQgBoep5ndDCyzg/+w
2Am75Y+7gkJMD5R66AvJPlL7M2oVA39sUlGMJl8993vZnTm03pyG/fw8WPP3Em7a
oIvLX5U0oAsaEZBxlWXolgSykwumvFe9Scu4eDlbRWf13hImCYybkkX/cZKExUZO
wios3n/h2V7Na+SVRFegfNSVuqo55qjUNFckiU0UsodfUagWqjjbcHvEQmpxbolZ
eQQEIx1Y67ZgseqGmc80lyBmdIj9/N1dbaWZaK8fRr2RWvIIlin2z0TIYWmyeCta
bipg74hmRpOkRkAvtXhWnWcDfyYiSqXGfXDsmt1mNLcbVzl9HnUl2vSD7oGt4ve9
nQ0I4dX1buLpF/g7cnRIvdTb6LKjUWzSnlK8pUZa2Ke0Xajd8lihAMHGYz4Jrzvf
fIr77w3k0Ng6hEV+aQ9BBgEbngvcx6PUp8xW2DKXi0lo+p1Ja2veRfvAxQARAQAB
tBxNYXJsaW5NciA8YWRtaW5AbWFybGlubXIuc2U+iQJUBBMBCgA+FiEEWT0Jcqr8
7Yp3/Tp9V1QozHCbpXAFAmZLnosCGwMFCQPCZwAFCwkIBwIGFQoJCAsCBBYCAwEC
HgECF4AACgkQV1QozHCbpXB4SA//evaANowPgaCuzMRJ4gulAFSfKYbgwDKoGe7U
8qMyenkt1zSeb+yM03916JDnDvRRhuDp0l65w9JMpjSTEiAG7Y4ItY8TRzmzw6/2
aJx0FcFeAtlXb+uYyBEfctbMdAeA9/f5MBb0yTPklwECrKqGdds3uD30Yzo6UyDM
3fbVS2rhUY9QE4UTxoxdkix0iP5V7k1iGwc2ggtAjcgcrfNqTXo0LyooqK3qmgcp
sO2DeAsFQegyLUw5HDFaEnsJejBBB4xjSyXJF2yHoqKLb5LJjmI3AsSgROHYDp03
eJQcT4KyHViiakwnhqI8PLR8H7HK4XNdmOz3XU/P+hbNe3yvkf06xWcohp4ECk8E
bDpxnaTQ/8LdbPOLLgmdtainzxw8fJghlLvRKil6N4M2Y9dRJV8aY13e4LZ0eW8P
AhN/NZZfb+vZ8RyOBaxgcUQPsD8V90AvkV381vKI4+n1zzsavEgpiMFwhvn/2qRv
LNbZJ5S9BB6oJxoRDs6Gde9qhjjnoE/Y9I37YAJfQvyX4BciK9Rsw6NH6xfoxq8k
gFentQULxVoaTmhv+exSoLwCm9yzZggUQlnjd8PxCeKKpV7BSnWIsgcpldgfWKS5
fNmN5BZ8OeXg8aIBJkZJNIpolUPYGFnXxM03KCWIwHQdc3F+14M6er6M+QYzFN8w
DjRZe6I=
=79Sm
-----END PGP PUBLIC KEY BLOCK-----
```

## Links

[Source](https://gitlab.com/MarlinMr/home)
